﻿using UnityEngine;
using System.Collections;


//A Simple Example of Implementation
[AddComponentMenu ("CustomScripts/TriggerSystem/Triggers/AreaTrigger")]
public class AreaTrigger : ObjectTrigger {

	void OnTriggerEnter2D(Collider2D collider)
	{
		Debug.Log ("WOOP");
		IsTriggered ();
	}

	void OnTriggerExit2D(Collider2D collider)
	{
		Debug.Log ("Boop");
		IsTriggered ();
	}

	public override void IsTriggered()
	{
		foreach (ObjectTriggerResponder R in Responder)
		{
			R.OnTrigger();
		}
	}
}
