﻿using UnityEngine;
using System.Collections;

/*
 * Attach Main camera to the script
 * Add a layer name 'Landscape' at position 8
*/

[AddComponentMenu("CustomScripts/PlayerStuff/Controller")]
public class Controller : MonoBehaviour 
{
	public float moveStepSize_ = 10f;
	public float jumpForce_ = 25f;
	public float maxVelocity_ = 20f;
	
	private float sqrMaxVelocity_;
	
	public Camera camera_;
	public Vector3 cameraOffset_ = new Vector3(0f, 0f, -5f);
	
	private bool grounded_;
	
	
	// Use this for initialization
	void Start () 
	{
		sqrMaxVelocity_ = maxVelocity_ * maxVelocity_;
	}
	
	void Update()
	{
		float hMov = Input.GetAxis("Horizontal");
		if(Mathf.Abs(hMov) > 0.1f)
		{
			Vector2 vel = rigidbody2D.velocity;
			vel.x += hMov * moveStepSize_ * Time.deltaTime;
			vel.x = Mathf.Abs(vel.x) > maxVelocity_ ? vel.x/Mathf.Abs(vel.x) * maxVelocity_ : vel.x;

			rigidbody2D.velocity = vel;
			//rigidbody2D.AddForce(new Vector2((hMov * moveStepSize_), 0f));
			//transform.Translate((hMov * moveStepSize_ * Time.deltaTime), 0f, 0f);
			
		}
		
		//if(hMov < 0)
			//rigidbody2D.AddForce(new Vector2((hMov * moveStepSize_), 0f));
		//	transform.Translate((hMov * moveStepSize_ * Time.deltaTime), 0f, 0f);
		if(Input.GetButton("Jump") && grounded_)
		{
			rigidbody2D.AddForce(new Vector2(0.0f, jumpForce_));
			//rigidbody2D.AddForce(new Vector2(0f, jumpForce_));
			grounded_ = false;
		}
		rigidbody2D.AddForce(new Vector2(0.00001f,0.0f));
		//Mathf.Clamp(rigidbody2D.velocity, 0f, maxSpeed_);
		camera_.transform.position = transform.position + cameraOffset_;
	}
	
	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.layer == 8)
		{
			grounded_ = true;
		}
	}
	
	void FixedUpdate()
	{
//		rigidbody2D.AddForce(Vector2.zero);
//		float mag = rigidbody2D.velocity.magnitude;
//		float X = rigidbody2D.velocity.x/mag ;
//		//if(v.sqrMagnitude > sqrMaxVelocity_)
//		{
//			rigidbody2D.velocity = new Vector2(X * maxVelocity_, rigidbody2D.velocity.y);
//		}
	}
}
