﻿using UnityEngine;
using System.Collections;

public class ObjectSwapController : MonoBehaviour {

    public PulseReceiver mainWorldObject;
    public PulseReceiver otherWorldObject;

    public bool active = false;

    public void Activate()
    {
        mainWorldObject.PulseEnter();
        otherWorldObject.PulseEnter();

        active = true;
    }

    public void Deactivate()
    {
        mainWorldObject.PulseExit();
        otherWorldObject.PulseExit();

        active = false;
    }
}
