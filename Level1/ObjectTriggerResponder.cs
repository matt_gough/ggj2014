﻿using UnityEngine;
using System.Collections;

[AddComponentMenu ("CustomScripts/TriggerSystem/Responders/ObjectTriggerResponder")]
public abstract class ObjectTriggerResponder : MonoBehaviour {

	public ObjectTrigger[] Trigger;

	public abstract void OnTrigger();
}
