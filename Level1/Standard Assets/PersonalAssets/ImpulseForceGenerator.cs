﻿using UnityEngine;
using System.Collections;

[AddComponentMenu ("CustomScripts/Environment/ImpulseForceGenerator")]
public class ImpulseForceGenerator : MonoBehaviour {
	Vector2 Direction;
	public float Magnitude = 1000;
	// Use this for initialization
	void Start () {
		Direction = transform.up;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		GameObject CollidingObject = collider.collider2D.gameObject;
		if (CollidingObject.tag == "Player") 
		{
			CollidingObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			CollidingObject.GetComponent<Rigidbody2D>().AddForce(Direction * Magnitude);
		}
	}
}
