﻿using UnityEngine;
using System.Collections;

public class CheckpointTrigger : ObjectTrigger {

	GameObject player;

	void OnTriggerEnter2D(Collider2D collision)
	{
		if ( collision.gameObject.tag == "Player")
		{
			player = collision.gameObject;
			IsTriggered();
		}
	}

	public override void IsTriggered()
	{	
		player.GetComponent<PlayerBehaviour> ().Save ();
	}
}
