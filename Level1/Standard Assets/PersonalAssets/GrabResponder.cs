﻿using UnityEngine;
using System.Collections;

//A Simple example of Implementation
[AddComponentMenu ("CustomScripts/TriggerSystem/Responders/GrabResponder")]
public class GrabResponder : ObjectTriggerResponder {

	public GameObject Target;
	bool ObjectActive = false;

	public override void OnTrigger()
	{
		ObjectActive = !ObjectActive;
		if (ObjectActive)
			transform.parent = Target.gameObject.transform;
		else
			transform.parent = null;


	}
}
