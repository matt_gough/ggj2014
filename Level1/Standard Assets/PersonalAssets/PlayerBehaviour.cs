﻿using UnityEngine;
using System.Collections;

[AddComponentMenu ("CustomScripts/PlayerStuff/PlayerBehaviour")]
public class PlayerBehaviour : MonoBehaviour {
	
	SaveGameData saveFile = new SaveGameData();
	// Use this for Initialization
	void Start () {
		SetUp ();

	}

	void SetUp()
	{
		SaveLoad.Load (ref saveFile);
		transform.position = new Vector3 (saveFile.PositionX, saveFile.PositionY, 0);
	}

	public void Save()
	{
		saveFile.Initialise(transform.position.x, transform.position.y, Application.loadedLevelName);
		SaveLoad.Save (saveFile);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = transform.position;
		if (pos.y < -10) {
			TakeDamage();
		}
		if (Input.GetButtonDown ("Fire1")) {
			Save ();
		}
	}

	public void TakeDamage()
	{
		Respawn ();
	}

	void Respawn()
	{
		Application.Quit();
		Application.LoadLevel(Application.loadedLevelName);
	}

}
