﻿using UnityEngine;
using System.Collections;


//A Simple Example of Implementation
[AddComponentMenu ("CustomScripts/TriggerSystem/Triggers/AreaTrigger")]
public class AreaTrigger : ObjectTrigger {

	void OnTriggerEnter2D(Collider2D collider)
	{
		IsTriggered ();
	}

	void OnTriggerExit2D(Collider2D collider)
	{
		IsTriggered ();
	}

	public override void IsTriggered()
	{
		foreach (ObjectTriggerResponder R in Responder)
		{
			R.OnTrigger();
		}
	}
}
