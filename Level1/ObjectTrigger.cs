﻿using UnityEngine;
using System.Collections;

[AddComponentMenu ("CustomScripts/TriggerSystem/Triggers/ObjectTrigger")]
public abstract class ObjectTrigger : MonoBehaviour {

	public ObjectTriggerResponder[] Responder;

	public abstract void IsTriggered();
}
