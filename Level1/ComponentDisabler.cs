using UnityEngine;
using System.Collections;

public class ComponentDisabler : ObjectTriggerResponder {
	
	public Component[] components;
	public void Start()
	{
		foreach (Component C in components)
		{
			C.active = true;
		}
	}
	
	public override void OnTrigger()
	{
		foreach ( Component C in components)
		{
			Component.Destroy(C);
		}
	}
}
