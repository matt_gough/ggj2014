﻿Shader "Custom/LambertAO" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_TransparencyOverride ("Transparency Override", Range(0.0, 1.0)) = 1.0	
		}
	SubShader {
		Tags { "RenderType"="Transparent" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert alpha

		sampler2D _MainTex;
		float _TransparencyOverride;
		
		struct Input {
			float2 uv_MainTex;
			float4 color : COLOR;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb * IN.color.a;
			o.Alpha = c.a * _TransparencyOverride;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
