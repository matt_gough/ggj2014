﻿using UnityEngine;
using System.Collections;

public class PulseReceiver : MonoBehaviour
{
    public float fadeInRate = 2;
    public float fadeOutRate = 0.2f;
    bool fadingIn = false;
    bool fadingOut = false;
    public float fadeOutTime = 3;
    public Component[] pulseControlledComponants;
    public bool activeByDefault = true;
    public bool pulseHit = false;

	void Start () 
    {
        //If object shouldn't be active by default, deactivate
        if (!activeByDefault)
        {
            //gameObject.collider.isTrigger = !activeByDefault;
            //gameObject.renderer.enabled = activeByDefault;
            foreach (Component com in pulseControlledComponants)
            {
                Collider2D col = com as Collider2D;
                if (col)
                    col.isTrigger = !activeByDefault;

                Renderer ren = com as Renderer;
                if (ren)
                {
                    if (!activeByDefault)
                    {
                        Color color = renderer.material.color;
                        color.a = 0;
                        renderer.material.color = color;
                    }
                }

                Rigidbody2D rb = com as Rigidbody2D;
                if (rb)
                    rb.isKinematic = !activeByDefault;

                MonoBehaviour mon = com as MonoBehaviour;
                if (mon)
                    mon.enabled = activeByDefault;
            }
        }
	}
	
	void Update () 
    {
        if (fadingOut)
            FadeOut();
        if (fadingIn)
            FadeIn();
	}

    private void FadeOut()
    {
        float rate;
        if (activeByDefault)
            rate = fadeInRate;
        else
            rate = fadeOutRate;
        Color color = renderer.material.color;
        if (color.a > 0)
            color.a -= rate * Time.deltaTime;
        else
            fadingOut = false;

        renderer.material.color = color;
    }

    private void FadeIn()
    {
        float rate;
        if (!activeByDefault)
            rate = fadeInRate;
        else
            rate = fadeOutRate;
        Color color = renderer.material.color;
        if (color.a < 1)
            color.a += rate * Time.deltaTime;
        else
            fadingIn = false;

        renderer.material.color = color;
    }

    public void PulseEnter()
    {
        if (activeByDefault)
            fadingOut = true;
        else
            fadingIn = true;
        pulseHit = true;
        //gameObject.collider.isTrigger = activeByDefault;
        //gameObject.renderer.enabled = !activeByDefault;
        foreach (Component com in pulseControlledComponants)
        {
            Collider2D col = com as Collider2D;
            if (col)
                col.isTrigger = activeByDefault;

            Rigidbody2D rb = com as Rigidbody2D;
            if (rb)
                rb.isKinematic = activeByDefault;

            MonoBehaviour mon = com as MonoBehaviour;
            if (mon)
                mon.enabled = !activeByDefault;
        }
    }

    public void PulseExit()
    {
        //gameObject.collider.isTrigger = !activeByDefault;
        //gameObject.renderer.enabled = activeByDefault;
        StartCoroutine(WaitToExit());
        pulseHit = false;
        if (activeByDefault)
            fadingIn = true;
        else
            fadingOut = true;
    }

    IEnumerator WaitToExit()
    {
        yield return new WaitForSeconds(fadeOutTime);

        foreach (Component com in pulseControlledComponants)
        {
            Collider2D col = com as Collider2D;
            if (col)
                col.isTrigger = !activeByDefault;

            Rigidbody2D rb = com as Rigidbody2D;
            if (rb)
                rb.isKinematic = !activeByDefault;

            MonoBehaviour mon = com as MonoBehaviour;
            if (mon)
                mon.enabled = activeByDefault;
        }
    }
}
