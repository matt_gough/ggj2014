﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RevealingCircleScript : MonoBehaviour {

	public bool Active = false;
	public float Size = 5.0f;

	public float Duration = 5.0f;

	float time = 0.0f;
    float modifier = 0.0f;

	SpriteRenderer sprRnd = null;

    //A list to store current pulse collisions
    List<Collider2D> currColliders = new List<Collider2D>();

	// Use this for initialization
	void Start () {
		time = 0.0f;
		Active = false;
		sprRnd = gameObject.GetComponent<SpriteRenderer>();
		if(sprRnd != null)
		{
			sprRnd.enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.F) && !Active)
            Active = true;

		if(Active)
		{
            float radius = 2 * modifier;
            Debug.DrawLine(transform.position, transform.position + Vector3.right * radius);
            List<Collider2D> hitList = new List<Collider2D>(Physics2D.OverlapCircleAll(transform.position, radius));
            for (int i = hitList.Count-1; i >= 0; i--)
            {
                Collider2D col = hitList[i];
                if (!currColliders.Contains(col))
                {
                    Debug.Log("Hit something new, adding to list");
                    currColliders.Add(col);
                    PulseReceiver r = col.GetComponent<PulseReceiver>();
                    if (r != null)
                    {
                        Debug.Log("Activating object");
                        if (!r.pulseHit)
                            r.PulseEnter();
                    }
                }
            }

            for (int i = currColliders.Count-1; i >= 0; i--)
            {
                Collider2D col = currColliders[i];
                if (!hitList.Contains(col))
                {
                    Debug.Log("No longer hitting something, removing to list");
                    currColliders.Remove(col);
                    PulseReceiver r = col.GetComponent<PulseReceiver>();
                    if (r != null)
                    {
                        Debug.Log("Deactivating object");
                        if (r.pulseHit)
                            r.PulseExit();
                    }
                }
            }

			if(sprRnd != null && !sprRnd.enabled)
			{
				sprRnd.enabled = true;
			}

			UpdateImpl();
		}
        else {
            foreach (Collider2D col in currColliders)
            {
                PulseReceiver r = col.GetComponent<PulseReceiver>();
                if (r != null)
                    r.PulseExit();
            }
            currColliders = new List<Collider2D>();
        }
	}

	public void Activate()
	{
		if(Active || sprRnd == null)
			return;

		Debug.Log("Activating!");

		time = 0.0f;
        modifier = 0;
		Active = true;
		sprRnd.material.SetFloat("_TimePassed", time);
		gameObject.transform.localScale = new Vector3(1,1,1);
	}

	private void UpdateImpl()
	{
		time += Time.fixedDeltaTime;

		if(sprRnd != null)
		{	
			sprRnd.material.SetFloat("_TimePassed", time);
		}

		modifier = (0.5f * (1 + Mathf.Sin(time - Mathf.PI * 0.5f))) * Size;
		gameObject.transform.localScale = new Vector3(modifier, modifier, modifier);

		//Debug.Log(modifier);
		if( time >= Duration )
		{
			time = 0.0f;
			Active = false;
			gameObject.transform.localScale = new Vector3(1,1,1);
			if(sprRnd != null)
			{
				sprRnd.enabled = false;
				sprRnd.material.SetFloat("_TimePassed", time);
			}
		}
	}
}
