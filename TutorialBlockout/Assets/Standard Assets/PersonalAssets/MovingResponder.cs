using UnityEngine;
using System.Collections;

[AddComponentMenu ("CustomScripts/TriggerSystem/Responders/MovingResponder")]
public class MovingResponder : ObjectTriggerResponder 
{
	public GameObject Destination;
	Vector3 Direction;
	bool Moving = false;
	float Speed = 10.0f;

	public override void OnTrigger()
	{
		Moving = true;
		Direction = (Destination.transform.position - transform.position).normalized;
	}

	void Update()
	{
		if (Moving) 
		{
			transform.position += Direction * Time.deltaTime * Speed;

			float Distance = Vector3.Distance(transform.position, Destination.transform.position);
			if( Distance < (Speed * Time.deltaTime)) 
				Moving = false;
		}
	}
}
