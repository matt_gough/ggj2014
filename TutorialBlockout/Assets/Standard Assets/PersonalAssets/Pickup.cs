﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {

	int ID;
	bool ShowMessage = false;
	float Duration = 5;

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "Player")
			PickedUp();
	}

	public void PickedUp()
	{
		ShowMessage = true;
		Duration += Time.time;
	}

	void OnGUI ( )
	{
		if ( ShowMessage )
		{
			GUI.Label(new Rect(Screen.width * 0.5f - 50f, Screen.height * 0.5f - 10f, 100f, 20f), "Biatch");
		}

		if(Time.time > Duration)
		{
			ShowMessage = false;
			this.Destroy(this);
		}
	}
}
