﻿using UnityEngine;
using System.Collections;

public class ComponentActivator : ObjectTriggerResponder {

	public Component[] components;
	public void Start()
	{
		foreach (Component C in components)
		{
			C.active = false;
		}
	}

	public override void OnTrigger()
	{
		foreach ( Component C in components)
		{
			C.active = true;
		}
	}
}
