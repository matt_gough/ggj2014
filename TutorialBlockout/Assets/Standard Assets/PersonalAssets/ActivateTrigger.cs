﻿using UnityEngine;
using System.Collections;

public class ActivateTrigger : ObjectTrigger {
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay2D(Collider2D collider)
	{
		if(collider.gameObject.tag == "Player")
		{
			if(Input.GetButtonDown("Fire1"))
				IsTriggered();
		}
	}

	public override void IsTriggered()
	{
		foreach (ObjectTriggerResponder R in Responder)
		{
			R.OnTrigger();
		}
	}
}
