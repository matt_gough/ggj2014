﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour 
{
	public GUISkin guiSkin;
	public Texture2D background, LOGO;
	public bool DragWindow = false;
	public string[] CreditsTextLines = new string[0];

	public string clicked = "", MessageDisplayOnCredits = "Credits \n\n ";
	private Rect WindowRect = new Rect((Screen.width / 2) - 100, Screen.height / 2, 200, 200);
	private float volume = 1.0f;
	
	private void Start()
	{
		MessageDisplayOnCredits += "Press Esc To Go Back\n\n\n\n";
		for (int x = 0; x < CreditsTextLines.Length;x++ )
		{
			MessageDisplayOnCredits += CreditsTextLines[x] + " \n\n ";
		}
	}
	
	private void OnGUI()
	{
		if (background != null)
			GUI.DrawTexture(new Rect(0,0,Screen.width , Screen.height),background);
		if (LOGO != null && clicked != "credits")
			GUI.DrawTexture(new Rect((Screen.width / 2) - 100, 30, 200, 200), LOGO);
		
		GUI.skin = guiSkin;
		//Draws the outline menu for the options screen
		if (clicked == "options") //CHANGE THIS
		{
			WindowRect = GUI.Window(1, WindowRect, optionsFunc, "Options");
		}
		//Draws the outline menu for the credits screen
		else if (clicked == "credits")
		{
			GUI.Box(new Rect (0,0,Screen.width,Screen.height), MessageDisplayOnCredits);
		}

		//Handles the drawing for all the available resolutions (FULLSCREEN ONLY)
		else if (clicked == "resolution")
		{
			GUILayout.BeginVertical();
			for (int x = 0; x < Screen.resolutions.Length;x++ )
			{
				if (GUILayout.Button(Screen.resolutions[x].width + "X" + Screen.resolutions[x].height))
				{
					Screen.SetResolution(Screen.resolutions[x].width,Screen.resolutions[x].height,true);
				}
			}
			GUILayout.EndVertical();

			//Handles the back button for resolution section
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Back"))
			{
				clicked = "options";
			}
			GUILayout.EndHorizontal();
		}
	}
	
	private void optionsFunc(int id)
	{
		if (GUILayout.Button("Resolution"))
		{
			clicked = "resolution";
		}
		GUILayout.Box("Volume");
		volume = GUILayout.HorizontalSlider(volume ,0.0f,1.0f);
		AudioListener.volume = volume;
		//Handles Back button for ROOT options location
		if (GUILayout.Button("Back"))
		{
			clicked = "";
		}
		if (DragWindow)
			GUI.DragWindow(new Rect (0,0,Screen.width,Screen.height));
	}
	
	private void menuFunc(int id)
	{
//		//buttons 
//		if (GUILayout.Button("Options"))
//		{
//			clicked = "options";
//		}
//		if (GUILayout.Button("Credits"))
//		{
//			clicked = "credits";
//		}
//		if (DragWindow)
//			GUI.DragWindow(new Rect(0, 0, Screen.width, Screen.height));
	}
	
	private void Update()
	{
		if (clicked == "credits" && Input.GetKey (KeyCode.Escape))
			clicked = "";
	}
}