﻿using UnityEngine;
using System.Collections;

public class LevelTraverseTrigger : MonoBehaviour 
{
	public Collider2D col_;
	public string name_;

	private bool guiActive_;

	// Use this for initialization
	void Start () 
	{
		col_.name = name_;
	}

	void OnGUI()
	{
		if(guiActive_)
			GUI.Label(new Rect(Screen.width/2, Screen.height/2, 100,100), "Press 'E' to Interact");
	}

	void OnTriggerStay2D(Collider2D col)
	{
		guiActive_ = true;

		bool button = Input.GetButton("Use");

		if(col_.name == "Quit" && button)
		{
			Application.Quit();
		}
		else if(col_.name == "Start" && button)
			Application.LoadLevel("Aslan Test lvl");
		else if(col_.name == "Options" && button)
		{
			GameObject obj = GameObject.Find("Scene Manager Object");
			Menu other = (Menu) obj.GetComponent(typeof(Menu));
			other.clicked = "options";
		}
		else if(col_.name == "Credits" && button)
		{
			GameObject obj = GameObject.Find("Scene Manager Object");
			Menu other = (Menu) obj.GetComponent(typeof(Menu));
			other.clicked = "credits";
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		guiActive_ = false;
	}
}
