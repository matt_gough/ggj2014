using UnityEngine;
using System;

[AddComponentMenu ("CustomScripts/AI/Chaser")]
public class Chaser : AI
{
	public float Radius, Speed;


	protected override void IdleCode()
	{
		if(TargetInRange())
			NextState = AIStates.ATTACKING;
	}
	protected override void FollowCode(){}
	protected override void PatrolCode(){}
	protected override void AttackCode()
	{
		float DeltaTime = Time.deltaTime;
		transform.position +=  (Target.transform.position - transform.position).normalized * Speed * DeltaTime;

		if (!TargetInRange ())
			NextState = AIStates.IDLE;
	}

	bool TargetInRange()
	{
		if(Vector2.Distance(Target.transform.position, transform.position) < Radius)
		   return true;
		return false;
	}

	void OnCollisionEnter2D(Collision2D collider)
	{
		if(Target == collider.collider.gameObject)
		{
			if(Target.tag == "Player")
				Target.GetComponent<PlayerBehaviour>().TakeDamage ();
		}
	}

	public override void HasPulsed()
	{
		if (CurrentState == AIStates.FOLLOWING)
			NextState = AIStates.ATTACKING;
	}
}

