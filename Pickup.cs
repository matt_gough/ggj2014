﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {

	int ID;
	bool ShowMessage = false;

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "Player")
			PickedUp();
	}

	public void PickedUp()
	{
		ShowMessage = true;
		yield WaitForSeconds (5000 );
		displayMessage = false;
	}

	void OnGUI ( )
	{
		if ( ShowMessage )
		{
			GUI.Label(new Rect(Screen.width * 0.5f - 50f, Screen.height * 0.5f - 10f, 100f, 20f), "Biatch");
		}
	}
}
