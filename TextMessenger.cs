﻿using UnityEngine;
using System.Collections;

public class TextMessenger : MonoBehaviour 
{
	public GameObject player_;
	//public GameObject objectAttached_;
	public Texture texture_;

	private bool faded_ = false;
	private Rect rect_;

	// Use this for initialization
	void Start () 
	{
		var screenPos = Camera.main.WorldToScreenPoint(transform.position);

		Vector3 tempVec = transform.position;
		//rect_ = new Rect(screenPos.x, Screen.height - screenPos.y - 96, 96, 64);
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	void OnGUI()
	{
		//guiText.text = "Test text";

		GUI.enabled=true;
		Camera cam=Camera.current;
		Vector3 pos=cam.WorldToScreenPoint(transform.position);
		GUI.Label(new Rect(pos.x,Screen.height-pos.y,150,130),"test");

		//GUI.DrawTexture(rect_, texture_); 

		if(Vector2.Distance(player_.transform.position, transform.position) < 30f)
		{
			GUI.DrawTexture(new Rect(pos.x - 14,Screen.height-pos.y - 102,32,32), texture_); 
		}
	}
}
