﻿using UnityEngine;
using System.Collections;

public class Moveable : MonoBehaviour 
{

	public bool leftRight_ = false;
	public bool upDown_ = false;

	public float distance_ = 3f;

	private Vector3 originalPos_;

	// Use this for initialization
	void Start () 
	{
		originalPos_ = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 tempVec = Vector3.zero;

		if(leftRight_)
			tempVec += new Vector3(Mathf.Sin(Time.time) * distance_, 0f, 0f);
		if(upDown_)
			tempVec += new Vector3(0f, Mathf.Sin(Time.time) * distance_, 0f);

		transform.position = originalPos_ + tempVec;

	}
}