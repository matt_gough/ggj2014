﻿using UnityEngine;
using System.Collections;

public class MapObjectToParent : MonoBehaviour 
{
	private Vector3 oldPos_ = Vector3.zero;
	private bool isMoving_ = true;
	Collider2D col_;

	void FixedUpdate()
	{
		Debug.Log(isMoving_);
		if(oldPos_ != gameObject.transform.position && !isMoving_)
		{
			isMoving_ = true;
			col_.transform.parent = null;
		}

		oldPos_ = gameObject.transform.position;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		col_ = col;
		col.transform.parent = gameObject.transform;
		isMoving_ = false;
	}
}
