﻿Shader "Custom/CelShadingShaderTransparent" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_OutlineColor ("Outline Color", Color) = (0,0,0,1)
		_UnlitOutlineThickness( "Unlit Outline thickness", Float) = 0.5
		_LitOutlineThickness( "Lit Outline thickness", Float) = 0.5
		_TransparencyOverride ("Transparency Override", Range(0.0, 1.1)) = 1.1	
	}
	SubShader {
		Tags { "RenderType"="Transparent" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf LambertCelShaded alpha

		sampler2D _MainTex;
		half4 _OutlineColor;
		float _UnlitOutlineThickness;
		float _LitOutlineThickness;

		float _TransparencyOverride;
		
		half4 LightingLambertCelShaded(SurfaceOutput s, half3 lightdir, half3 viewDir, half atten)
		{
			half NdotL = max(dot(s.Normal, lightdir), 0.0);
			/*half4 c = half4(0.3176, 0.34117, 0.36078, 1);
			
			if(NdotL >= 0.7 && NdotL <= 0.85)
			{
				c = half4(0.43137, 0.43529, 0.47843, 1);
			}
			else if(NdotL >= 0.55 && NdotL < 0.7)
			{
				c = half4(0.6353, 0.6862, 0.7215, 1);
			}
			else if(NdotL >= 0.3 && NdotL < 0.55)
			{
			  c = half4(0.7843, 0.81568, 0.8196, 1);
			}
			*/

			float cel_shade = 1.0;

			if(NdotL >= 0.4 && NdotL <= 0.7)
				cel_shade = 0.6;
			else if(NdotL < 0.4)
				cel_shade = 0.35;

			bool outline = false;
			if(dot(viewDir, s.Normal) < lerp(_UnlitOutlineThickness, _LitOutlineThickness, NdotL))
				outline = true;

			half4 colour;
			colour.rgb = s.Albedo * _LightColor0.rgb;
			colour.rgb *= cel_shade;
			/* colour.r = 1.0 - (1.0 - colour.r) * (1.0 - c.r);
			colour.g = 1.0 - (1.0 - colour.g) * (1.0 - c.g);
			colour.b = 1.0 - (1.0 - colour.b) * (1.0 - c.b); */
		
			colour.a = s.Alpha;

			if(outline)
				colour = _OutlineColor;

			return colour;
		}

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a * _TransparencyOverride;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
