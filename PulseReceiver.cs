﻿using UnityEngine;
using System.Collections;
using System;

public class PulseReceiver : MonoBehaviour
{
    public float fadeInRate = 2;
    public float fadeOutRate = 0.2f;
    bool fadingIn = false;
    bool fadingOut = false;
    public float fadeOutTime = 3;
    public Component[] pulseControlledComponants;
    public bool activeByDefault = true;
    public bool pulseHit = false;

    float alpha_ = 1f;
    string shaderName_;

	void Start () 
    {
        shaderName_ = renderer.material.shader.name;
        //If object shouldn't be active by default, deactivate
        if (!activeByDefault)
        {
            //gameObject.collider.isTrigger = !activeByDefault;
            //gameObject.renderer.enabled = activeByDefault;
            foreach (Component com in pulseControlledComponants)
            {
                Collider2D col = com as Collider2D;
                if (col)
                    col.isTrigger = !activeByDefault;

                Renderer ren = com as Renderer;
                if (ren)
                {
                    if (!activeByDefault)
                    {
                    //     Color color = renderer.material.color;
                    //     color.a = 0;
                    //     renderer.material.color = color;
                        alpha_ = 0;
                        renderer.material.SetFloat("_TransparencyOverride", alpha_);
                    }
                }

                Rigidbody2D rb = com as Rigidbody2D;
                if (rb)
                    rb.isKinematic = !activeByDefault;

                MonoBehaviour mon = com as MonoBehaviour;
                if (mon)
                    mon.enabled = activeByDefault;
            }
        }
	}
	
	void Update () 
    {
        if(alpha_ <= 0.9999)
        {
        //     Texture texture = renderer.material.mainTexture;
        //     renderer.material = new Material(Shader.Find("Custom/LambertAOTransparent"));
        //     renderer.material.mainTexture = texture;
           
            Shader shader1 =  Shader.Find("Custom/LambertAO");
            if(renderer.material.shader == shader1)
            {

                renderer.material.shader = Shader.Find("Custom/LambertAOTransparent");
            }

            Shader shader2 =  Shader.Find("Custom/CelShadingShader");
            if(renderer.material.shader == shader2)
            {

                renderer.material.shader = Shader.Find("Custom/CelShadingShaderTransparent");
            }
            //if(shader != null)
        }
        else
         {
        //     Texture texture = renderer.material.mainTexture;
        //     renderer.material = new Material(Shader.Find("Custom/LambertAO"));
        //     renderer.material.mainTexture = texture;
            
            Shader shader1 =  Shader.Find("Custom/LambertAOTransparent");
            if(renderer.material.shader == shader1)
            {

                renderer.material.shader = Shader.Find("Custom/LambertAO");
            }

            Shader shader2 =  Shader.Find("Custom/CelShadingShaderTransparent");
            if(renderer.material.shader == shader2)
            {

                renderer.material.shader = Shader.Find("Custom/CelShadingShader");
            }
        }

        if (fadingOut)
            FadeOut();
        if (fadingIn)
            FadeIn();
	}

    private void FadeOut()
    {
        float rate;
        if (activeByDefault)
            rate = fadeInRate;
        else
            rate = fadeOutRate;
        if (alpha_ > 0)
            alpha_ -= rate * Time.deltaTime;
        else
            fadingOut = false;

        renderer.material.SetFloat("_TransparencyOverride", alpha_);
    }

    private void FadeIn()
    {
        float rate;
        if (!activeByDefault)
            rate = fadeInRate;
        else
            rate = fadeOutRate;
        if (alpha_ < 1)
            alpha_ += rate * Time.deltaTime;
        else
            fadingIn = false;

        renderer.material.SetFloat("_TransparencyOverride", alpha_);
    }

    public void PulseEnter()
    {
        if (activeByDefault)
            fadingOut = true;
        else
            fadingIn = true;
        pulseHit = true;
        //gameObject.collider.isTrigger = activeByDefault;
        //gameObject.renderer.enabled = !activeByDefault;
        foreach (Component com in pulseControlledComponants)
        {
            Collider2D col = com as Collider2D;
            if (col)
                col.isTrigger = activeByDefault;

            Rigidbody2D rb = com as Rigidbody2D;
            if (rb)
                rb.isKinematic = activeByDefault;

            MonoBehaviour mon = com as MonoBehaviour;
            if (mon)
                mon.enabled = !activeByDefault;
        }
    }

    public void PulseExit()
    {
        //gameObject.collider.isTrigger = !activeByDefault;
        //gameObject.renderer.enabled = activeByDefault;
        StartCoroutine(WaitToExit());
        pulseHit = false;
        if (activeByDefault)
            fadingIn = true;
        else
            fadingOut = true;
    }

    IEnumerator WaitToExit()
    {
        yield return new WaitForSeconds(fadeOutTime);

        foreach (Component com in pulseControlledComponants)
        {
            Collider2D col = com as Collider2D;
            if (col)
                col.isTrigger = !activeByDefault;

            Rigidbody2D rb = com as Rigidbody2D;
            if (rb)
                rb.isKinematic = !activeByDefault;

            MonoBehaviour mon = com as MonoBehaviour;
            if (mon)
                mon.enabled = activeByDefault;
        }
    }
}
